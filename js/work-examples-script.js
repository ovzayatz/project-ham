'use strict';

function showContentOnTabClick(tabElementsList, tabElementsCollection, contentElementsCollection, loadMoreButton) {

    tabElementsCollection[0].classList.add('active');


    for (let c = 0; c < contentElementsCollection.length; c++) {
        contentElementsCollection[c].classList.add('hidden');

        for (let t = 0; t < tabElementsCollection.length; t++) {
            if (contentElementsCollection[c].dataset.workCategory.includes(tabElementsCollection[t].dataset.workCategory)) {
                contentElementsCollection[c].classList.remove('hidden');
                contentElementsCollection[c].classList.add('act-content')
            }
        }
    }
    const contentElementsAct = document.getElementsByClassName('act-content');
    for (let i = 0; i < tabElementsCollection.length; i++) {
        tabElementsCollection[i].addEventListener('click', (event) => {
            tabElementsList.getElementsByClassName('active')[0].classList.remove('active');
            event.target.classList.add('active');

            for (let a = 0; a < contentElementsCollection.length; a++) {
                contentElementsCollection[a].classList.remove('act-content');
                contentElementsCollection[a].classList.remove('act-hidden')
            }

            for (let j = 0; j < contentElementsCollection.length; j++) {

                if (contentElementsCollection[j].dataset.workCategory.includes(tabElementsCollection[i].dataset.workCategory)) {

                    contentElementsCollection[j].classList.remove('hidden');
                    contentElementsCollection[j].classList.add('act-content')

                } else {
                    contentElementsCollection[j].classList.add('hidden')
                }
            }

            for (let v = 0; v < contentElementsAct.length; v++) {
                if (v < (contentElementsAct.length - 12)) {
                    contentElementsAct[v + 12].classList.add('act-hidden');

                }
            }
            if (contentElementsActHidden.length === 0) {
                loadMoreButton.style.display = 'none';
            } else {
                loadMoreButton.style.display = 'flex';
            }
        })
    }


    for (let v = 0; v < contentElementsAct.length; v++) {
        if (v < (contentElementsAct.length - 12)) {
            contentElementsAct[v + 12].classList.add('act-hidden');
        }
    }

    const contentElementsActHidden = document.getElementsByClassName('act-hidden');
    if (contentElementsActHidden.length === 0) {
        loadMoreButton.style.display = 'none';
    } else {
        loadMoreButton.style.display = 'flex';
    }

    loadMoreButton.addEventListener('click', () => {
        const elementsToUnhide = [...contentElementsActHidden].slice(0, 12);
        elementsToUnhide.forEach((e) => {
            e.classList.remove('act-hidden');
            if (contentElementsActHidden.length === 0) {
                loadMoreButton.style.display = 'none';
            } else {
                loadMoreButton.style.display = 'flex';
            }
        })
    })
}


const workExamples = document.getElementsByClassName('work-example');
const workCategoriesTabs = document.getElementsByClassName('work-category-tab');
const categoriesTabsList = document.getElementsByClassName('work-categories-list')[0];
const loadMoreBtn = document.getElementById('load-more-btn');

showContentOnTabClick(categoriesTabsList, workCategoriesTabs, workExamples, loadMoreBtn);

