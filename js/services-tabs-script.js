'use strict';

function showContentOnTabClick(tabElementsCollection, contentElementsCollection) {

    tabElementsCollection[0].classList.add('active');

    for (let t = 0; t < contentElementsCollection.length; t++) {
        contentElementsCollection[t].style.cssText = 'display: none';
        contentElementsCollection[0].style.cssText = 'display: grid';
    }

    for (let i = 0; i < tabElementsCollection.length; i++) {
        tabElementsCollection[i].addEventListener('click', () => {

            for (let j = 0; j < contentElementsCollection.length; j++) {
                if (contentElementsCollection[j].dataset.serviceName === tabElementsCollection[i].dataset.serviceName) {

                    for (let t = 0; t < contentElementsCollection.length; t++) {
                        contentElementsCollection[t].style.cssText = 'display: none'
                    }
                    contentElementsCollection[j].style.cssText = 'display: grid';

                    for (let r = 0; r < tabElementsCollection.length; r++) {
                        tabElementsCollection[r].classList.remove('active');
                    }
                    tabElementsCollection[i].classList.add('active');
                }
            }
        })
    }
}


const tabsCollection = document.getElementsByClassName('service-tab');
const contentCollection = document.getElementsByClassName('service-tab-content');
showContentOnTabClick(tabsCollection, contentCollection);

